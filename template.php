<?php

// add jquery.pngFix.js file
drupal_add_js(drupal_get_path('theme', 'salamander') . '/js/jquery.pngfix.js', 'theme');


// Salamander's regions
function salamander_regions() {
  return array(
       'header' => t('header'),
       'content_top' => t('content top'),
       'sidebar_left' => t('left sidebar'),
       'sidebar_right' => t('right sidebar'),
       'content_bottom' => t('content bottom'),
       'footer' => t('footer'),
	   'user1' => t('user1'),
	   'user2' => t('user2'),
	   'user3' => t('user3'),
	   'user4' => t('user4'),
	   'user5' => t('user5'),
	   'user6' => t('user6')
       
  );
} 

 
//  Breadcrumb override
function salamander_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
	  $breadcrumb[] = drupal_get_title();  // RADUT's complete breadcrumb ( � = › , � = &#187; &raquo;)
    return '<div class="breadcrumb">'. implode(' &raquo; ', $breadcrumb) .'</div>';
  }
}


// Quick fix for the validation error: 'ID "edit-submit" already defined'
$elementCountForHack = 0;
function phptemplate_submit($element) {
  global $elementCountForHack;
  return str_replace('edit-submit', 'edit-submit-' . ++$elementCountForHack, theme('button', $element));
}

