
Salamander 6.x-1.0, 2007-10-22

------------------------------
- Drupal 6 update


======================================================

Salamander 5.x-1.3, 2007-09-02
------------------------------
- add dynamic second breadcrumb


Salamander 5.x-1.2, 2007-08-30
------------------------------
- complete redesign of the header
- fixed a few small CSS bugs


Salamander 5.x-1.1, 2007-08-27
------------------------------
- fix jQuery errors in IE6


Salamander 5.x-1.0, 2007-08-27
------------------------------
- first official release
- fixed a few small bugs
- some changes throughout the code base


salamander 5.x-1.x-dev, 2007-08-22
----------------------------------
- nightly development snapshot from HEAD
